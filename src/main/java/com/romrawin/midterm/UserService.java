/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.romrawin.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class UserService {
    private static ArrayList<Userproduct> productList = new ArrayList<>();
    private static Userproduct currentProduct = null;
    static {
        load();
    }
    //Create (C)
    public static boolean addProduct(Userproduct product) {
        productList.add(product);
        save();
        return true;
    }

    //Delete (D)
    public static boolean delProduct(Userproduct product) {
        productList.remove(product);
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }

    // Read (R)
    public static ArrayList<Userproduct> getProduct() {
        return productList;
    }

    public static Userproduct getProduct(int index) {
        return productList.get(index);
    }

    //Update (U)
    public static boolean updateProduct(int index, Userproduct product) {
        productList.set(index, product);
        save();
        return true;
    }
    //File

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("bank.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("bank.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Userproduct>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
